<?php

namespace Tutorial\Train\Block\Test;

use Magento\Framework\View\Element\Template\Context;
use Tutorial\Train\Model\DepartmentFactory;

/**
 * Class TestListData
 * @package Tutorial\Train\Block\Test
 */
class TestListData extends \Magento\Framework\View\Element\Template
{

    /**
     * TestListData constructor.
     * @param Context $context
     * @param DepartmentFactory $test
     */
    public function __construct(
        Context $context,
        DepartmentFactory $test
    )
    {
        $this->_test = $test;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\View\Element\Template
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('List Department'));
        return $this;
    }


    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getTestCollection()
    {
        $test = $this->_test->create();
        $collection = $test->getCollection();
        return $collection;
    }

    /**
     * @return string
     */
    public function getTestUrlDepartment()
    {
        return $this->getUrl('train/test/newdata');
    }

}