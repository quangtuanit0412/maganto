<?php

namespace Tutorial\Train\Block\Test;

use Magento\Framework\View\Element\Template;

/**
 * Class TestNewData
 * @package Tutorial\Train\Block\Test
 */
class TestNewData extends Template
{
    /**
     * TestNewData constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('train/test/post', ['_secure' => true]);
    }
}
