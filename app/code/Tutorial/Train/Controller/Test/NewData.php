<?php

namespace Tutorial\Train\Controller\Test;

/**
 * Class NewData
 * @package Tutorial\Train\Controller\Test
 */
class NewData extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}