<?php

namespace Tutorial\Train\Controller\Test;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Tutorial\Train\Model\DepartmentFactory;
use Magento\Framework\Controller\ResultFactory;


/**
 * Class Post
 * @package Tutorial\Train\Controller\Test
 */
class Post extends \Magento\Framework\App\Action\Action
{

    /**
     * @var DepartmentFactory
     */
    protected $_test;
    /**
     * @var ResultFactory
     */
    Protected $_resultRedirect;

    /**
     * Post constructor.
     * @param Context $context
     * @param DepartmentFactory $test
     * @param ResultFactory $result
     */
    public function __construct(
        Context $context,
        DepartmentFactory $test,
        ResultFactory $result
    )
    {
        $this->_test = $test;
        $this->_resultRedirect = $result;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {

        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        $data = $this->validatedParams();
        $model = $this->_test->create();
        $model->setData(['name' => $data['name'], 'description' => $data['description']]);
        if ($model->save()) {
            $this->messageManager->addSuccessMessage(__('You saved the data.'));
        } else {
            $this->messageManager->addErrorMessage(__('Data was not saved.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('train/test/newdata');
        return $resultRedirect;

    }

    /**
     * @return array
     * @throws LocalizedException
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if ($request->getParam('name') === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if ($request->getParam('description') === '') {
            throw new LocalizedException(__('Enter the description and try again.'));
        }
        return $request->getParams();
    }
}